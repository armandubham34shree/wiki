---
title: Garuda Update
description: Garuda-update, the update script of Garuda Linux
published: true
date: 2022-05-17T17:07:31.984Z
tags: cheatsheet
editor: markdown
dateCreated: 2022-05-04T00:00:20.221Z
---

`garuda-update` is Garuda Linux's preferred update solution. The update script allows Garuda Linux to apply certain hotfixes and update automations for a smoother experience.

### What does Garuda Update do?
1. A self-update is performed to ensure the latest fixes are downloaded.
2. The mirrorlist is automatically refreshed using `rate-mirrors`
3. The archlinux and chaotic keyrings are updated
4. The `garuda-hotfixes` package is updated before any other system packages are updated to fix any important issues.
5. Package related hotfixes such as conflicts and other issues are automatically resolved after they have been approved by the developers.
6. The system update is applied. Common pacman prompts are automatically answered by "auto-pacman". The user is asked to answer with "y" if they want to apply the update.
7. Common post-update tasks are executed. The mlocate database is updated, fish completions and micro plugins are updated.
8. If needed, a changelog showing major configuration changes is shown.

### Usage

Simply use `garuda-update` or `update` in your shell of choice. Both commands do the same thing. Additionally, the following parameters are available:

|Parameter         |Environment variable|Description                              |
|------------------|--------------------|-----------------------------------------|
|--skip-mirrorlist |SKIP_MIRRORLIST=1   |Don't update the mirrorlist before applying the system update.|
|--aur/-a          |UPDATE_AUR=1        |Update AUR packages via paru/yay after system update.|
|--noconfirm       |PACMAN_NOCONFIRM=1  |Pass option --noconfirm to pacman|

#### Config file

A config file can be created at `/etc/garuda/garuda-update/config` that can contain the environment variables in the following format:
```
SKIP_MIRRORLIST=1
UPDATE_AUR=1
```